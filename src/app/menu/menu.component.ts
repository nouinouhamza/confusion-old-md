import { Component, OnInit } from '@angular/core';
import { DishService } from '../services/dish.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    dishes;

    selectedDish;

    constructor(private dishService: DishService) { }

    ngOnInit() {
     this.dishService.getDishes()
      .subscribe(dishes =>  this.dishes = dishes);
    }

  onSelect(dish){
      this.selectedDish = dish;
  }

}
